
## 0.1.15

#### Bug Fixes

#### Features

1.ymate指令，增加忽略功能。详见 [ymate控制器生成](./docs/ymate.md) 

## 0.1.11

#### Bug Fixes

#### Features

1.增加ymate指令，用于生成ymate控制器代码。详见 [ymate控制器生成](./docs/ymate.md) 

## 0.1.10

#### Bug Fixes

1. 解决windows环境生成失败问题.

#### Features

1.增加misc目录
2.config.json移至misc目录
3.优化生成输出内容
4.调整模版格式，使其更便于二次开发

## 0.1.9

#### Bug Fixes

#### Features

1.generator savePath默认参数改为./src

## 0.1.7

#### Bug Fixes

#### Features

1.详情页面生成
2.生成逻辑优化

更新预告

0.2.0

增加可视化配置界面

## 0.1.6

#### Bug Fixes

#### Features

1.查询及编辑完成