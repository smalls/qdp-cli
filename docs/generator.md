# 生成代码

```bash
qdp generator
```

[可选参数]

|参数|类型|描述|
|:-:|:--:|:--|
|-c,--config|string|配置文件，默认为./config.json|
|-s,--savePath|string|代码保存路径，默认为./src|
