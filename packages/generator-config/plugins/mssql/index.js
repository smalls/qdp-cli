const IAgent = require('../../lib/Agent');
const mssql = require('mssql')

const SQL = `SELECT  table_name = d.name ,
  table_comment = ISNULL(f.[value], '') ,
  col_order  = a.colorder,
  [col_name] = a.name ,
  col_comment = ISNULL(g.[value], '') ,
  [col_type] = CASE WHEN b.name IN ( 'varchar', 'nvarchar' )
    THEN b.name + '('
      + CAST(COLUMNPROPERTY(a.id, a.name, 'PRECISION') AS VARCHAR(4))
      + ')'
    WHEN b.name = 'decimal'
    THEN b.name + '('
      + CAST(COLUMNPROPERTY(a.id, a.name, 'PRECISION') AS VARCHAR(4))
      + ','
      + CAST(COLUMNPROPERTY(a.id, a.name, 'Scale') AS VARCHAR(4))
      + ')'
    ELSE b.name
    END
  FROM syscolumns a
  LEFT JOIN systypes b ON a.xusertype = b.xusertype
  INNER JOIN sysobjects d ON a.id = d.id AND d.xtype = 'U' AND d.name <> 'dtproperties'
  LEFT JOIN sys.extended_properties g ON a.id = g.major_id AND a.colid = g.minor_id
  LEFT JOIN sys.extended_properties f ON d.id = f.major_id AND f.minor_id = 0
  ORDER BY a.id , a.colorder`;

module.exports = class MsSqlPlugin extends IAgent {
  async query() {
    mssql.on('error', err => { console.log(err) })
    await mssql.connect(this.Context.config);
    const res = await mssql.query(SQL);
    await mssql.close();
    if (res && res.recordsets) {
      return res.recordsets[0]
    } else {
      return [];
    }
  }
}