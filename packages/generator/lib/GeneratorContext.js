const { toHump, toCamel, getVersions, merge } = require('../../cli-shared-utils/');
const fs = require('fs');
const getWebApiMethod = (value) => {
  return ["query", "detail"].some(p => value.indexOf(p) > -1) ? "GET" : "POST";
}
const isNotEditor = (field) => {
  return ['_index', '_selection'].some(p => field.name == p);
}
/**
 * @param {Array} apis
 * @param {object} entity
 */
const generatorAPI = (apis, entity) => {
  const { key, value, pageName, name } = entity;
  if (apis.some(p => p.name == value || p.value == value || p.name == value.name || p.value == value.path)) {
    return apis.find(p => p.name == value || p.value == value || p.name == value.name || p.value == value.path)
  }
  let apiName = name || value.name || toHump([key, pageName.replace(/-/g, '_')].join('_'));
  let apiValue = value.path || value;
  let method = value.method || getWebApiMethod(apiValue);
  return { name: apiName, value: apiValue, method, isNew: true }
}

module.exports = class GeneratorContext {
  constructor(options) {
    this.version = "1.0"
    this.options = options;

    this.extra = {
      date: new Date().toLocaleString(),
      author: `qdp-cli`,
      version: getVersions().current
    }
  }

  /**
   * 导入配置文件
   * @param {string} configFile 配置文件路径
   */
  importConfig(configFile) {
    this.configFile = configFile;
    const data = fs.readFileSync(configFile);
    const defaultConfigs = {
      dict: {},
      pages: [],
      default: {
        uploadScript: ""
      },
      webapi: {
        mock: false,
        apis: {}
      }
    }
    try {
      let jsonString = data.toString();
      jsonString = jsonString
        .replace(/\/\*\*.+?\*\//g, '')
        // 清除空白行（对代码没影响）
        .replace(/\n[ ]*\n/g, '\n')
        // .replace(/\r|\n|\t/g, '')
        .replace(/\'/g, '&#39;');
      // this.config = require(configFile);
      this.config = merge(defaultConfigs, JSON.parse(jsonString));
      this.parse();
    } catch (e) {
      console.log();
      console.error(e);
      console.log();
    }
  }

  /** 解析配置文件 */
  parse() {
    this.__parse__pages__();
    this.__set__path__();
    this.__parse__webapi__();
    this.__parse__field__();
    this.__parse__dict__();
  }

  /** 解析页面 */
  __parse__pages__() {
    const webpages = [];
    const webgroups = [];
    const contextPowers = [];
    const fetch = (pages, parents = []) => {
      pages.forEach(pageOption => {

        if (!pageOption.path) {
          pageOption.path = pageOption.name.replace(/-/g, '/');
        }

        if (pageOption.children) {
          const defaultWebGroup = {
            hideInBread: false,
            hideInMenu: false,
            children: [],
            isGroup: false,
            sort: 0
          }
          const webgroup = merge(defaultWebGroup, pageOption);
          webgroup.children = webgroup.children.map(p => p.name);
          webgroups.push(webgroup);
          fetch(pageOption.children, [...parents, pageOption]);
        } else {
          // 编辑页面默认参数
          const defaultModifyPage = {
            // 页面参数
            page: {
              // 页面宽度
              width: 550,
              labelWidth: 140
            },
            params: {

            },
            // 字段参数
            field: {
              // 字段所占格数 参考 grid 布局，将一行平均分为24分
              span: 24,
              // 是否为必填项
              required: false
            }
          };
          // 检索页面默认参数
          const defaultSearchPage = {
            operation: {
              fixed: "right",
              width: 0
            },
            paper: {
              enable: true
            },
            filters: {},
            actions: [],
            canModify: false,
            canCreate: false,
            canRemove: false
          };
          // 根据 api 判断权限
          const permissions = Object.keys(pageOption.apis);
          const powers = permissions
            .filter(p => ['query', 'detail'].indexOf(p) == -1)
            .map(action => `POWERS.${action.toUpperCase()}`);

          if (pageOption.search && pageOption.search.actions) {
            pageOption.search.actions.forEach(action => {
              action.name = action.code.replace(/-/g, '_').toUpperCase();
              action.power = `POWERS.${action.name}`;
              let power = contextPowers.find(cp => cp.code === action.code);
              if (!power) {
                contextPowers.push({ ...action });
              }
              powers.push(action.power);
            });
          }
          pageOption.single = pageOption.single || true;
          pageOption.primary = pageOption.primary || 'id';

          pageOption.route = {
            hideInBread: false,
            hideInMenu: false,
            sort: 0,
            parents: parents.map(p => {
              const { name, title, icon, path } = p;
              return { name, title, icon, path };
            }),
            powers: ['POWERS.BROWSER', ...powers]
          }

          pageOption.modify = merge(defaultModifyPage, pageOption.modify);
          let modifyParams = pageOption.modify.params;
          pageOption.modify.params = Object.keys(modifyParams).map(p => {
            return { name: p, value: modifyParams[p] }
          });

          const search = merge(defaultSearchPage, pageOption.search);

          const actions = search.actions || [];
          actions.forEach(p => { !p.code && (p.power = p.name) });

          search.actions = actions;
          search.defaultActions = actions.filter(p => !p.mode || p.mode == 'default');
          search.moreActions = actions.filter(p => p.mode && p.mode == 'more');
          search.canModify = permissions.indexOf('update') > -1;
          search.canCreate = permissions.indexOf('create') > -1;
          search.canRemove = permissions.indexOf('remove') > -1;

          pageOption.detailable = permissions.indexOf('detail') > -1;
          pageOption.editable = search.canModify || search.canCreate;

          pageOption.search = search;

          webpages.push(pageOption);
        }
      })
    }
    fetch(this.config.pages);

    // 处理 webpage
    this.webpages = webpages;

    this.powers = contextPowers;

    // 处理 webgroup
    webgroups.forEach(g => {
      g.children = g.children.map(c => webpages.find(p => p.name == c));
    });
    this.webgroups = webgroups;
  }

  /** 解析字段 */
  __parse__field__() {
    const fieldEditorDefaults = {
      index: 0,
      required: false
    }
    const defaultOptions = this.config.default;
    this._fetch_webpage(webpage => {
      const { fields, modify, search } = webpage;
      const fieldModifyOptions = merge(fieldEditorDefaults, modify.field);
      let columns = [];
      if (fields) {
        columns = fields.map(field => {
          // 处理详情（detail）
          let detail = field.detail;
          if (detail == true) {
            if (webpage.detailable) {
              detail = {
                primary: webpage.primary,
                route: `${webpage.name}-detail`
              }
            } else {
              detail = undefined;
            }
          }
          // 处理编辑（editor）
          let editor = field.editor;
          if (isNotEditor(field)) {
            // _index 或 _selection 字段不需要编辑
            editor = false;
          } else if (editor == false) {
            // 其他不需要编辑的字段
          } else if (editor != undefined) {
            editor = merge(fieldModifyOptions, { ...editor });
          } else {
            editor = { ...fieldModifyOptions }
          }
          // 处理关联（relation）
          let relation = field.relation;
          if (relation && relation.path) {
            let name = "";
            const value = relation.path || '';
            if (value) {
              // 查看webapi.apis中是否已经定义该api
              let webapi = this.webapi.apis.find(p => p.value == value)
              if (webapi) {
                // 已定义 -> 直接使用name
                relation.name = webapi.name;
              } else {
                // 未定义 -> 生成驼峰式名称
                name = toHump(value.split('/').reverse().join('_'))
                this.webapi.apis.push({ name, path: value, method: "GET" });
                relation.name = name;
              }
            } else {
              relation = undefined;
            }
          }
          // 处理过滤（filter）
          let filter = field.filter;
          if (filter) {
            if (field.type === 'date' && filter.range == undefined) {
              filter.range = true;
            }
          }
          return {
            ...field,
            filter,
            cname: field.name.toLowerCase(),
            editor,
            detail,
            relation
          }
        });
      }
      webpage.fields = undefined;
      search.columns = columns.filter(p => !p.hidden);
      if (search.blurSearch != false) {
        search.blurSearch = true;
      }
      // 编辑页面是否含有file类型的字段
      modify.hasUpload = columns.some(p => p.type === 'file');
      modify.uploadScript = modify.uploadScript || defaultOptions.uploadScript;
      // 处理 检索条件
      const filters = columns
        .filter(p => p.filter)
        .map(p => { return { ...p, defaultValue: '``' } })
        .sort((a, b) => (a.filter.index || 999) - (b.filter.index || 999));
      let searchFilters = webpage.search.filters;
      Object.keys(searchFilters).forEach(p => {
        filters.push({ name: p, defaultValue: searchFilters[p] });
      });

      search.filters = {
        all: filters,
        region: filters.filter(p => p.type == 'date' && p.filter.range).map(p => p),
        normal: filters.filter(p => p.filter && p.filter.mode == 'default'),
        advance: filters.filter(p => p.filter && (!p.filter.mode || p.filter.mode == 'advance')),
        relations: filters.filter(p => p.relation)
      }

      // 处理 可编辑内容
      modify.editors = columns
        .filter(p => p.editor)
        .sort((a, b) => (a.editor.index || 999) - (b.editor.index || 999));
      // 处理 需要包装的可编辑内容
      modify.packageable = modify.editors
        .filter(p => p.type == 'date' || p.type == 'money');
      // 处理 字段验证规则
      modify.rules = modify.editors
        .filter(p => p.editor.required || p.type === 'password');
      // 处理 可编辑的数据字典
      modify.relations = modify.editors
        .filter(p => p.relation);

      // 统计同名relation出现次数
      let temp = {};
      modify.relations.forEach(p => {
        const name = p.relation.name;
        if (temp.hasOwnProperty(name)) {
          p.relation.ignore = true;
        } else {
          temp[name] = true;
        }

        if (p.params) {
          if (typeof p.params === 'object') {
            p.params = JSON.stringify(p.params, null).replace(/"/g, "'")
          }
        }
        if (p.searchParams) {
          if (typeof p.searchParams === 'object') {
            p.searchParams = JSON.stringify(p.searchParams, null).replace(/"/g, "'")
          }
        }
        if (p.editorParams) {
          if (typeof p.editorParams === 'object') {
            p.editorParams = JSON.stringify(p.editorParams, null).replace(/"/g, "'")
          }
        }
      });
      temp = {};
      search.filters.relations.forEach(p => {
        const name = p.relation.name;
        if (temp.hasOwnProperty(name)) {
          p.relation.ignore = true;
        } else {
          temp[name] = true;
        }
      });
    });
  }

  __parse__webapi__() {
    const apis = [];
    const defaultWebAPI = {
      mock: false,
      apis: {}
    }
    const webapi = Object.assign(defaultWebAPI, this.config.webapi);
    const o = webapi.apis;
    Object.keys(o).forEach(name => {
      apis.push(generatorAPI(apis, { name, value: o[name] }));
    });

    this._fetch_webpage(webpage => {
      if (!webpage.apis) {
        return;
      }
      Object.keys(webpage.apis).forEach(key => {
        let entity = generatorAPI(apis, { key, value: webpage.apis[key], pageName: webpage.name });
        webpage.apis[key] = entity;
        if (entity.isNew) {
          delete entity.isNew;
          apis.push({ ...entity });
        }
      });
    });
    this.webapi = {
      ...this.config.webapi,
      apis,
    }
  }

  __parse__dict__() {
    const dicts = [];
    const o = this.config.dict;
    Object.keys(o).forEach(dict => {
      dicts.push({
        name: dict,
        data: Object.keys(o[dict]).map(p => {
          return {
            value: p,
            text: o[dict][p]
          }
        })
      });
    });
    this.dicts = dicts;
  }

  _fetch_webpage(fn) {
    this.webpages.forEach(webpage => fn(webpage));
  }

  /** 统一初始化路径 */
  __set__path__() {
    this.webpages.forEach(webpage => {
      const { route } = webpage;
      const names = [...route.parents, webpage].map(p => toCamel(p.name));
      route.fullpath = names.join('/');
      route.folder = names.join('-');
      route.camelName = names[names.length - 1];
      route.searchName = `${route.camelName}Search`
      route.detailName = `${route.camelName}Detail`
      route.modifyName = `${route.camelName}Modify`
    });
  }
}
