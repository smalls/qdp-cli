exports.POWERS = {
  /** 访问-browser */
  BROWSER: { text: '访问', code: 'browser' },
  /** 新增-create */
  CREATE: { text: '新增', code: 'create', icon: "action-create" },
  /** 编辑-update */
  UPDATE: { text: '编辑', code: 'update', icon: "action-modify" },
  /** 删除-remove */
  REMOVE: { text: '删除', code: 'remove', icon: "action-remove" },
  /** 导出-export */
  EXPORT: { text: "导出", code: "export", icon: "action-export" },
  /** 启用-enable */
  ENABLE: { text: '启用', code: 'enable' },
  /** 禁用-disable */
  DISABLE: { text: '禁用', code: 'disable' },
  /** 重置密码-reset_password */
  RESET_PASSWORD: { text: '重置密码', code: 'reset_password', icon: "action-change-password" },
  /** 保存-save */
  SAVE: { text: '保存', code: 'save', icon: 'action-save' }
};