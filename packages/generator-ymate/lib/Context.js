const { getVersions, chalk } = require('../../cli-shared-utils');
const fs = require('fs');
const path = require('path');
module.exports = class Context {

  constructor(root, options) {
    this.version = "1.0"
    this.options = options;
    this.source = options.source || "./";
    this.root = root;
    this.extra = {
      date: new Date().toLocaleString(),
      author: `qdp-cli`,
      version: getVersions().current
    }

    this.parse();
  }

  parse() {
    this.entities = [];
    const FILE_EXTENSION = ".java";
    const { root } = this;
    const configfile = path.join(root, 'qdp.json');
    let config = {
      ignore: [],
      filter: ["*"],
      extra: {}
    };
    if (fs.existsSync(configfile)) {
      let text = fs.readFileSync(configfile, 'utf8');
      try {
        config = Object.assign(config, JSON.parse(text));
      } catch (e) {
        console.log();
        console.log(chalk.error(e.message));
        console.log();
        return;
      }
    }
    const some = (array, text) => {
      return array.some(s => {
        if (/%/.test(s)) {
          return new RegExp(s.replace(/%/g, '.')).test(text)
        } else {
          return text.replace(FILE_EXTENSION, '') == s
        }
      })
    }
    // 读取实体
    this.entities = fs.readdirSync(root)
      // 过滤文件类型
      .filter(p => p.endsWith(FILE_EXTENSION))
      // .filter(p => config.filter.includes("*") || some(config.filter, p))
      // .filter(p => !some(config.ignore, p))
      .map(file => this.__parse_entity(root, file))
      .filter(p => p);
    // 过滤需要生成对实体类
    this.entities.forEach(p => {
      p.buildable = (config.filter.includes("*") || some(config.filter, p.name)) && !some(config.ignore, p.name)
    });
    // 处理额外内容
    Object.keys(config.extra).forEach(entityName => {
      const entityExtra = config.extra[entityName];
      const entity = this.entities.find(e => e.name == entityName);
      this.__handle_extra(entity, entityExtra, this.entities);
    });

    // console.info(JSON.stringify(this.entities, null, 2));
  }

  /**
   * 解析实体
   * @param {string} root 路径
   * @param {string} name 实体名称
   */
  __parse_entity(root, name) {
    const entity = {
      /** 实体名称 */
      name: name.replace('.java', ''),
      /** 控制器文件名称 */
      fileName: name.replace('.', 'Controller.'),
      /** 控制器路由 */
      routeName: name
        .replace(/([A-Z])/g, (a, b) => '/' + (b.toLowerCase()))
        .replace(/.java/, ''),
      /** 控制器类名 */
      className: name.replace('.java', 'Controller'),
      hasVO: false,
      entityExtra: {
        query: {
          conditions: [],
          fields: [],
          params: []
        }
      }
    };
    const file = path.resolve(root, name)
    const entityClass = fs.readFileSync(file, { encoding: 'utf8' });
    const REGEX_TABLE_NAME = /public static final String TABLE_NAME = "(.+?)";/g;

    let execute = REGEX_TABLE_NAME.exec(entityClass);
    if (execute == null) {
      return;
    }
    entity.table = execute[1];
    entity.tableAlias = "";

    const REGEX_PACKAGE = /package (.+?).model;/;
    execute = REGEX_PACKAGE.exec(entityClass);
    if (execute == null) {
      return;
    }
    entity.packageRoot = execute[1];

    const REGEX_COMMENT = /@Comment\("(.+?)"\)/g;
    const comments = [...entityClass.matchAll(REGEX_COMMENT)].map(p => p[1]);
    // const REGEX_FIELD = /@PropertyState\(propertyName = FIELDS.(.+?)\)/g;
    // const fields = [...entityClass.matchAll(REGEX_FIELD)].map(p => p[1]);

    const REGEX_SET = /public void set(.+?)\((.+?) .+?\)/g;
    let matchs = [...entityClass.matchAll(REGEX_SET)];
    // const NumbicType = ['Long', 'Integer', 'Double', 'Float', 'Decimal'];
    entity.fields = matchs.map((p, i) => {
      const name = p[1].replace(/^([A-Z])/, (a, b) => b.toLowerCase());
      const field = name.replace(/([A-Z])/g, "_$1").toLowerCase();
      const type = p[2].replace(/java.lang./, '');
      return {
        name,
        comment: comments[i],
        alias: p[1],
        // 不为 String 类型的全部视为数值类型
        // isRegion: NumbicType.includes(type),
        isRegion: type !== 'String',// NumbicType.includes(type),
        type,
        field
      }
    });
    entity.query = entity.fields.filter(p => p.type == 'String' && p.name != 'id');
    return entity;
  }

  /**
   * 实体额外内容处理
   * @param {object} entity 实体
   * @param {object} entityExtra 扩展内容
   * @param {object[]} entities 实体清单
   */
  __handle_extra(entity, entityExtra, entities) {
    if (!entity || !entityExtra.conditions) { return }
    // 关联字段
    const fields = [];
    // 关联条件
    const conditions = [];
    // 关联额外参数
    const params = [];
    entity.tableAlias = `_t_`;

    const parseRelation = (exp) => {
      // exp === 'SystemUser.id'
      let res = exp.split('.');
      let entity = entities.find(p => p.name === res[0]);
      if (!entity) {
        return {
          text: exp
        };
      } else {
        let field = entity.fields.find(p => p.field === res[1]);
        return {
          ...entity,
          ...field
        };
      }
    }

    const parseCondition = (relation, tableAlias) => {
      if (relation.table === entity.table) {
        return `${entity.tableAlias}.${relation.field}`
      } else if (relation.text) {
        return relation.text;
      } else {
        return `${tableAlias}.${relation.field}`;
      }
    }

    Object.keys(entityExtra.conditions).forEach(alias => {
      let conditionConfig = entityExtra.conditions[alias];
      let pr1 = parseRelation(conditionConfig.fields[0]);
      let pr2 = parseRelation(conditionConfig.fields[1]);
      let condition = {
        table: pr2.table,
        tableAlias: alias,
        join: (conditionConfig.join || 'inner').toUpperCase(),
        conditions: []
      };
      condition.conditions.push(parseCondition(pr1, alias));
      condition.conditions.push(parseCondition(pr2, alias));
      let and = conditionConfig.fields.slice(2);
      for (let i = 0; i < and.length; i += 2) {
        if (i < and.length - 1) {
          const exp = /'(.+?)'/;
          let res = parseCondition(parseRelation(and[i]), alias);
          if (exp.test(res)) {
            condition.conditions.push("?");
            params.push(res.match(exp)[1]);
          } else {
            condition.conditions.push(res);
          }
          res = parseCondition(parseRelation(and[i + 1]), alias);
          if (exp.test(res)) {
            condition.conditions.push("?");
            params.push(res.match(exp)[1]);
          } else {
            condition.conditions.push(res);
          }
        }
      }
      conditions.push(condition);
    });

    Object.keys(entityExtra.fields).forEach(fieldAlias => {
      let fieldExp = entityExtra.fields[fieldAlias];
      // let fieldParser = parseRelation(fieldExp);
      let res = fieldExp.split('.');
      let table = res[0];
      const condition = conditions.find(p => p.table === table || p.tableAlias === table);
      // const s = entities.find();
      // console.info("--condition---", table, JSON.stringify(condition, null, 2));
      if (condition) {
        let conditionEntity = entities.find(p => p.table === condition.table);
        // console.info("--conditionEntity---", JSON.stringify(conditionEntity, null, 2));
        if (!conditionEntity) { return; }
        let conditionField = conditionEntity.fields.find(p => p.field === res[1]);
        // console.info("--conditionField---", JSON.stringify(conditionField, null, 2));
        if (!conditionField) { return; }
        let { isRegion, type, field } = conditionField;
        let { comment } = conditionEntity;
        fields.push({
          isRegion, type, field, table, comment,
          tableAlias: condition.tableAlias,
          relationName: fieldAlias,
          relationAlias: fieldAlias.replace(/^([a-z])/, (a, b) => b.toUpperCase()),
          relationField: fieldAlias.replace(/([A-Z])/g, "_$1").toLowerCase()
        });
      }
    });

    entity.hasVO = fields.length > 0;
    entity.entityExtra = {
      query: {
        conditions,
        fields,
        params
      }
    }
  }

}
