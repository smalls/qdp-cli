
module.exports = class ActionResult {
  constructor() {
    this.property = {
      ret: 0,
      msg: undefined,
      data: undefined
    }
  }

  code(code) {
    this.property.ret = code;
    return this;
  }

  message(message) {
    this.property.msg = message;
    return this;
  }

  data(data) {
    this.property.data = data;
    return this;
  }

  /** 转为JSON格式的数据 */
  toJSON() {
    return JSON.stringify(this.property);
  }
}