const fs = require('fs')
const path = require('path');
module.exports = (savePath) => {
  const configPath = path.resolve(savePath, `config.json`);
  const templatePath = path.resolve(__dirname, "./config.json")
  const content = fs.readFileSync(templatePath);
  fs.writeFileSync(configPath, content);
  return configPath;
}