# QDP CLI 

通过配置文件生成前后端通用代码

## 基础功能

### 依赖

+ nodejs 运行环境
+ npm 用于安装nodejs依赖
+ git 用于生成模版

### 快速上手

```bash
echo "全局安装 QDP-CLI"
npm i -g qdp-cli
echo "创建工程（自动创建目录）"
qdp create qdp-demo && cd qdp-demo
echo "初始化 CRUD 配置文件"
qdp generator -i
echo "通过 CRUD 生成代码文件"
qdp generator
```

### 安装

```bash
npm i -g qdp-cli
```

### 使用帮助

```bash
qdp --help
```

[配置参考](./docs/config.md) 

[创建工程](./docs/create.md) 

[代码生成](./docs/generator.md) 

[ymate控制器生成](./docs/ymate.md) 
