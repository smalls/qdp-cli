const path = require('path')
const fs = require('fs')
const ejs = require('ejs')
const { mkdir } = require('../../cli-shared-utils/');
module.exports = class IGenerator {
  constructor(context) {
    this.context = context;
    this.ejs = ejs;
  }

  async generate(savePath) {
    this.savePath = path.resolve(this.context.options.cwd, savePath);
  }

  render(template, data = {}, ejsOptions = {}) {
    data = Object.assign({ ...data }, { extra: this.context.extra });
    return this.ejs.render(template, data, ejsOptions)
  }

  writeFile(filePath, content) {
    mkdir(path.dirname(filePath));
    fs.writeFileSync(filePath, content);
  }

}
