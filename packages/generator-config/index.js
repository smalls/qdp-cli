const fs = require('fs')
const path = require('path');
const Context = require('./lib/Context');
const { clearConsole, generateTitle, chalk, mkdir, finddir } = require('../cli-shared-utils/');

async function executor(options) {
  clearConsole(generateTitle());
  console.log();
  console.log(` ${chalk.cyan('Generator Config')} `);
  console.log();
  console.log(chalk.section('- command arguments'));
  console.log(`${chalk.gray(JSON.stringify(options, null, 2))} `);

  const modeldirs = finddir('./', 'model', ['node_modules']);
  console.log(modeldirs);

  // 配置
  var config = {
    server: "192.168.3.200",
    database: "BLT",
    user: "sa",
    password: "lgkj1234",
    port: 1433
  }

  const context = new Context(options, config);
  const mssql = require('./plugins/mssql');
  const agent = new mssql(context);

  const res = await agent.executor();

  console.log(JSON.stringify(res, null, 2));
  console.log();
  console.log(` ${chalk.success('successfully!')} `);
  console.log();
}

module.exports = (...args) => {
  return executor(...args).catch(err => {
    console.error(err);
    console.error(` ${chalk.red(err.message)}`);
    process.exit(1)
  })
}
