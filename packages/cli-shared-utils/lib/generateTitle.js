const { getVersions } = require('./getVersions')
const chalk = require('chalk')
exports.generateTitle = () => {
  const { current } = getVersions()
  let title = chalk.bold.cyan(`QDP CLI v${current}`);

  return title
}
