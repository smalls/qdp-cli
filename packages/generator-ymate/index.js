'use strict'
const Context = require('./lib/Context');
const { clearConsole, generateTitle, chalk, mkdir, finddir } = require('../cli-shared-utils');
const path = require('path');
const fs = require('fs');
const ejs = require('ejs')
async function executor(options) {
  clearConsole(generateTitle());
  console.log();
  console.log(` ${chalk.cyan('Generator ymate-webmvc Controller')} `);
  console.log();
  console.log(chalk.section('- command arguments'));
  console.log(`${chalk.gray(JSON.stringify(options, null, 2))} `);

  const modeldirs = finddir(options.source || './', 'model', ['node_modules', 'target']);
  if (modeldirs.length === 0) {
    console.error(` ${chalk.red('Directory \'model\' not found!')}`);
    return;
  }
  const modelDir = modeldirs[0];
  const controllerDir = path.resolve(modelDir, "../controller/crud");
  const viewObjectDir = path.resolve(modelDir, "../vo/crud");
  const context = new Context(modelDir, options);
  const baseDir = path.resolve(__dirname, "./template");
  const template = fs.readFileSync(path.resolve(baseDir, 'controller.ejs'), 'utf-8');
  const template_viewobject = fs.readFileSync(path.resolve(baseDir, 'view-object.ejs'), 'utf-8');

  if (context.entities.length > 0) {
    mkdir(controllerDir);
    const entities = context.entities.filter(p => p.buildable);
    // console.info(JSON.stringify(entities[0], null, 2));
    entities.forEach(entity => {
      const filePath = path.resolve(controllerDir, entity.fileName);
      const data = Object.assign({ ...entity }, { extra: context.extra });
      const content = ejs.render(template, data);
      fs.writeFileSync(filePath, content);
      console.info(`${entity.fileName}`);
    });
    if (entities.some(p => p.hasVO)) {
      mkdir(viewObjectDir);
      entities
        .filter(p => p.hasVO)
        .forEach(entity => {
          const filename = entity.name + "VO.java";
          const filePath = path.resolve(viewObjectDir, filename);
          const data = Object.assign({ ...entity }, { extra: context.extra });
          const content = ejs.render(template_viewobject, data);
          fs.writeFileSync(filePath, content);
          console.info(filename);
        });
    }
  }
  console.log();
  console.log(` ${chalk.success('successfully!')} `);
  console.log();
}

module.exports = (...args) => {
  return executor(...args).catch(err => {
    console.error(err);
    console.error(` ${chalk.red(err.message)}`);
    process.exit(1)
  })
}
