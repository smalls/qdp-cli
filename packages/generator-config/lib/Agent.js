const { toHump, getVersions } = require('../../cli-shared-utils/');
module.exports = class IAgent {
  constructor(context) {
    this.Context = context;
  }
  async query() { }

  async executor(){
    let pages = [];
    const res = await this.query();
    res.forEach(p => {
      const table_name = p.table_name.replace('BLT_', '');
      const tableName = toHump(table_name);
      const apiFix = table_name.replace(/_/g, '/')
      let page = pages.find(p => p.name == tableName);
      if (page == null) {
        page = {
          name: tableName,
          title: p.table_comment,
          icon: "",
          fields: [{ "name": "_index", "editor": false }],
          modify: {},
          apis: {
            query: `/${apiFix}/query`,
            remove: `/${apiFix}/remove`,
            create: `/${apiFix}/create`,
            update: `/${apiFix}/modify`,
            detail: `/${apiFix}/detail`
          }
        };
        pages.push(page);
      }
      const fieldName = toHump(p.col_name);
      page.fields.push({
        name: fieldName,
        text: p.col_comment || fieldName,
      });
    });
    return pages;
  }
}