
const path = require('path');
const fs = require('fs');
const chalk = require('chalk');
const modulesFiles = fs.readdirSync(path.resolve(__dirname, './lib'));
modulesFiles.forEach(m => {
  Object.assign(exports, require(`./lib/${m}`))
});

chalk.section = (text) => {
  return chalk.bold.cyan(text);
}
chalk.path = (text) => {
  return chalk.italic.gray(text);
}
chalk.error = (text) => {
  return chalk.red(text);
}
chalk.success = (text) => {
  return chalk.greenBright(text);
}
exports.chalk = chalk;
exports.semver = require('semver')
