const GeneratorInterface = require('../../lib/GeneratorInterface')
const path = require('path')
const fs = require('fs')
let basePath;
const { chalk } = require('../../../cli-shared-utils');
module.exports = class VueIViewGenerator extends GeneratorInterface {

  constructor(context) {
    super(context);
    this.baseDir = path.resolve(__dirname, "./template");
    basePath = context.config.target || 'apps/crud';
    this.ejs.fileLoader = (f) => {
      const dir = path.resolve(this.baseDir, f);
      const s = path.resolve(this.baseDir, '/');
      const realPath = path.resolve(this.baseDir, dir.substring(s.length));
      return fs.readFileSync(realPath);
    };
  }
  /**
   * 生成
   * @param {string} targetPath 保存路径
   */
  async generate(targetPath) {
    super.generate(targetPath);

    console.log(chalk.section('- renderer'));
    console.log(chalk.success('  vue-iview-generator v0.4'));

    this.templates = {
      lib: {
        api: fs.readFileSync(path.resolve(this.baseDir, 'lib/api.ejs'), 'utf-8'),
        mock: fs.readFileSync(path.resolve(this.baseDir, 'lib/mock.ejs'), 'utf-8'),
        dict: fs.readFileSync(path.resolve(this.baseDir, 'lib/dict.ejs'), 'utf-8'),
        route: fs.readFileSync(path.resolve(this.baseDir, 'lib/route.ejs'), 'utf-8')
      },
      pages: {
        search: fs.readFileSync(path.resolve(this.baseDir, 'pages/search.ejs'), 'utf-8')
      }
    }
    const { webpages, configFile } = this.context;
    const { savePath } = this;
    const appsPath = path.resolve(savePath, basePath);

    console.log();
    console.log(chalk.section('- environment'));
    console.log(` [config file]  ${chalk.path(configFile)} `);
    console.log(` [save path  ]  ${chalk.path(savePath)} `);
    console.log(` [base path  ]  ${chalk.path(appsPath)} `);

    console.log();
    console.log(chalk.section('- render API'));
    this.__render_api__(appsPath, this.context);
    console.log();
    console.log(chalk.section('- render ROUTE'));
    this.__render_route__(appsPath, this.context);
    console.log();
    console.log(chalk.section('- render DICT'));
    this.__render_dict__(appsPath, this.context);
    console.log();
    console.log(chalk.section('- render PAGES'));

    webpages.forEach(webpage => {
      this.__render_page__(appsPath, webpage)
    });
  }
  /**
   * 渲染 API
   * @param {string} targetPath 保存路径
   * @param {GeneratorContext} context 生成器上下文
   */
  __render_api__(targetPath, context) {
    const { webapi } = context;
    let template = this.templates.lib.api;
    let realPath = path.resolve(targetPath, `lib/api.js`);
    let content = this.render(template, { webapi });
    this.writeFile(realPath, content);
    console.log(`  ${chalk.path(realPath.replace(this.savePath, ''))} `);
    if (webapi.mock) {
      const { webpages } = context;
      template = this.templates.lib.mock;
      realPath = path.resolve(targetPath, `lib/mock.js`);
      content = this.render(template, { webapi, webpages });
      this.writeFile(realPath, content);
      console.log(`  ${chalk.path(realPath.replace(this.savePath, ''))} `);
    }
  }
  /**
   * 渲染 数据字典
   * @param {string} targetPath 保存路径
   * @param {GeneratorContext} context 生成器上下文
   */
  __render_dict__(targetPath, context) {
    const { dicts } = context;
    const template = this.templates.lib.dict;
    const realPath = path.resolve(targetPath, `lib/dict.js`);
    const content = this.render(template, { dicts });
    this.writeFile(realPath, content);
    console.log(`  ${chalk.path(realPath.replace(this.savePath, ''))} `);
  }
  /**
   * 渲染 路由
   * @param {string} targetPath 保存路径
   * @param {GeneratorContext} context 生成器上下文
   */
  __render_route__(targetPath, context) {
    const template = this.templates.lib.route;
    const realPath = path.resolve(targetPath, `lib/route.js`);
    const { webgroups } = context;
    const content = this.render(template, { webgroups });
    this.writeFile(realPath, content);
    console.log(`  ${chalk.path(realPath.replace(this.savePath, ''))} `);
  }
  /**
   * 渲染 页面
   * @param {string} targetPath 保存路径
   * @param {GeneratorContext} context 生成器上下文
   */
  __render_page__(targetPath, webpage) {
    console.log();
    console.log(chalk.section(`- ${webpage.name} - ${webpage.title}`));
    this.__render_search__(targetPath, webpage)
  }
  /**
   * 渲染 检索页面
   * @param {string} targetPath 保存路径
   * @param {GeneratorContext} context 生成器上下文
   */
  __render_search__(targetPath, webpage) {
    const template = this.templates.pages.search;
    const { fullpath } = webpage.route;
    const realPath = path.resolve(targetPath, `pages/${fullpath}/index.vue`);
    if (!webpage.apis) {
      console.log(`  ${chalk.yellowBright('未设置api参数，跳过该页面生成')} `);
      return;
    }
    const content = this.render(template, webpage);
    this.writeFile(realPath, content);
    console.log(`  ${chalk.path(realPath.replace(this.savePath, ''))} `);
  }
}

// <% '脚本' 标签，用于流程控制，无输出。
// <%_ 删除其前面的空格符
// <%= 输出数据到模板（输出是转义 HTML 标签）
// <%- 输出非转义的数据到模板
// <%# 注释标签，不执行、不输出内容
// <%% 输出字符串 '<%'
// %> 一般结束标签
// -%> 删除紧随其后的换行符
// _%> 将结束标签后面的空格符删除
