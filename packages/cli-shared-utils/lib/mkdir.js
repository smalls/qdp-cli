const path = require('path')
const fs = require('fs')
const mkdir = (dirname) => {
  if (fs.existsSync(dirname)) {
    return true;
  } else {
    if (mkdir(path.dirname(dirname))) {
      fs.mkdirSync(dirname);
      return true;
    }
  }
}

const finddir = (source, name, ignore = []) => {
  const res = [];
  if (fs.existsSync(source) && fs.statSync(source).isDirectory()) {
    const dirs = fs.readdirSync(source)
      .filter(p => ignore.indexOf(p) == -1);
    dirs.filter(p => p == name)
      .forEach(p => {
        res.push(path.resolve(source, p));
      });
    dirs.forEach(p => {
      res.push(...finddir(path.resolve(source, p), name, ignore));
    });
  }
  return res;
}
exports.mkdir = mkdir;
exports.finddir = finddir;
