---
sidebar: auto
---

# 配置参考 | Configuration Reference

## PAGES

## API

## DICT

```
{
  /** 系统模块定义 */
  "pages": [
    {
      /** 功能代码：保存的路径 */
      "name": "system",
      /** 功能路径：功能访问路径，默认为name */
      "path": "",
      /** 功能名称：显示在菜单中的名称 */
      "title": "系统功能",
      /** 功能图标：显示在菜单中的图标（iconfont） */
      "icon": "management-drxx10",
      /** 功能子集 */
      "children": [
        {
          "name": "user",
          "title": "用户",
          "icon": "management-drxx88",
          /** 数据主键：默认为'id' */
          "primary": "",
          /** 是否为单页应用：默认为true，当single=false时，独立新增及编辑路由 */
          "single": false,
          /** 数据项 */
          "fields": [
            { "name": "_index" },
            { "name": "userName", "text": "用户名称", "detail": true, "filter": { "index": 1 }, "editor": { "index": 1, "required": true } },
            { "name": "nickName", "text": "用户昵称", "filter": true, "editor": true },
            { "name": "permissionName", "text": "权限名称", "detail": { "primary": "permissionId", "route": "permission-detail" } },
            { "name": "permissionId", "text": "权限", "hidden":true, "editor": true, "type":"selector", "relation":{ "path":"permission/query", "key":"id", "value":"name" } },
            { "name": "mobile", "text": "手机号码", "type": "mobile", "filter": { "index": 3 } },
            { "name": "type", "text": "类型", "type":"selector", "dict":"user_type", "filter": { "index": 2 }, "editor": { "index": 2, "required": true } },
            { "name": "creator", "text": "创建者", "format":"v => v || \"系统\"" },
            { "name": "createTime", "text": "创建时间", "type": "date", "dataFormat":"yyyy-MM-dd", "filter": true },
            { "name": "lastModifyTime", "text": "更新时间", "type": "date", "filter": { "index": 1, "mode":"default" } },
            { "name": "remark", "text": "备注", "type":"textarea", "sort":false , "tooltip": true, "editor": true }
          ],
          /** 查询页面配置项 */
          "search":{
            /** 列表[操作]功能配置项 */
            "operation":{
              /** 操作项宽度 */
              "width":340,
              /**
               * 操作项浮动位置，为空标识不浮动 默认为right，
               * 可选值 left|right
               */
              "fixed":""
            },
            /** 列表分页功能配置项 */
            "paper":{
              /** 是否允许分页 */
              "enable": false
            },
            /** 配置列表附加功能 */
            "actions":[
              /**
              * 功能项
              * name      string  功能项名称：对应api
              * mode      string  功能项显示的位置，['default','more']，默认为[default]
              *                   default 显示在 [编辑/删除] 之后
              *                   more    显示在 [更多] 中
              * confirm   string  操作确认的提示信息，该值被设置后功能为需要确认的操作。
              * before    boolean 是否需要控制功能显示的前置条件，默认为false
              */
              { "name": "enable", "mode": "more" },
              { "name": "disable", "mode": "more" },
              { "name": "detail" },
              { "name": "export", "confirm":"确定" , "before": true }
            ]
          },
          /** 接口项，根据接口列表生成权限代码 */
          "api": {
            "query": "user/query",
            "remove": "user/remove",
            "create": "user/create",
            "update": "user/modify",
            "detail": "user/detail",
            "export": "user/export",
            "enable": "user/status/enable",
            "disable": "user/status/disable"
          }
        },
        {
          "name": "permission",
          "title": "权限",
          "icon": "management-drxx45",
          "path": "permission",
          "fields": [
            { "name": "_index" },
            { "name": "name", "text": "权限名称", "detail": true, "editor": true, "filter": true },
            { "name": "createTime", "text": "创建时间", "type": "date", "filter": { "index": 1, "mode":"default" } },
            { "name": "lastModifyTime", "text": "更新时间", "type": "date", "filter": true },
            { "name": "remark", "text": "备注", "tooltip": true }
          ],
          "api": {
            "query": "permission/query",
            "remove": "permission/remove",
            "create": "permission/create",
            "update": "permission/modify",
            "detail": "permission/detail",
            "export": "permission/export",
            "enable": "permission/status/enable",
            "disable": "permission/status/disable"
          }
        }
      ]
    },
    {
      "name": "maintain",
      "title": "系统运维",
      "icon": "management-drxx33",
      "children": [
        {
          "name": "logger",
          "title": "监控日志",
          "primary": "lid",
          "fields": [
            { "name": "_index" },
            { "name": "business", "text": "业务", "type":"selector", "dict": "business_category", "filter": true },
            { "name": "action", "text": "动作", "type":"selector", "dict": "action_category", "filter": true },
            { "name": "clientIp", "detail": true, "text": "访问来源", "width": 150 },
            { "name": "threadId", "text": "线程号", "width": 100 },
            { "name": "operatorName", "text": "操作员", "format":"v => v || \"系统\"" },
            { "name": "createTime", "text": "创建时间", "type": "date", "filter": { "index": 1, "mode":"default" } },
            { "name": "content", "text": "内容", "tooltip": true, "width": 400, "sort":false },
            { "name": "remark", "text": "备注", "tooltip": true }
          ],
          "api": {
            "query": "logger/query",
            "remove": "logger/remove",
            "detail": "logger/detail"
          }
        },{
          "name": "service",
          "title": "服务日志",
          "fields": [
            { "name": "_index" },
            { "name": "business", "text": "业务", "type":"selector", "dict": "business_category", "filter": true },
            { "name": "action", "text": "动作", "type":"selector", "dict": "action_category", "filter": true },
            { "name": "clientIp", "text": "访问来源", "width": 150 },
            { "name": "threadId", "text": "线程号", "width": 100 },
            { "name": "operatorName", "text": "操作员", "format":"v => v || \"系统\"" },
            { "name": "createTime", "text": "创建时间", "type": "date", "filter": { "index": 1, "mode":"default" } },
            { "name": "content", "text": "内容", "tooltip": true, "width": 400, "sort":false },
            { "name": "remark", "text": "备注", "tooltip": true }
          ],
          "api": {
            "query": "logger/query",
            "remove": "logger/remove",
            "detail": "logger/detail"
          }
        }
      ]
    }
  ],
  /** 接口服务定义 */
  "api":{
    "account_login":"account/login",
    "account_logout":"account/logout",
    "account_validate":"account/validate"
  },
  /** 数据字典定义 */
  "dict":{
    "user_type":{
      "0":"类型一",
      "1":"类型二",
      "2":"类型三",
      "3":"类型四"
    },
    "user_type2":{
      "A":"类型一",
      "B":"类型二",
      "C":"类型三"
    }
  }
}
```