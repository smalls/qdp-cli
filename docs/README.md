---
home: true
heroImage: /hero.png
heroText: Hero 标题
tagline: Hero 副标题
actionText: 快速上手 →
actionLink: /guide/
features:
- title: 简洁至上
  details: 以 Markdown 为中心的项目结构，以最少的配置帮助你专注于写作。
- title: Vue驱动
  details: 享受 Vue + webpack 的开发体验，在 Markdown 中使用 Vue 组件，同时可以使用 Vue 来开发自定义主题。
- title: 高性能
  details: VuePress 为每个页面预渲染生成静态的 HTML，同时在页面被加载的时候，将作为 SPA 运行。
footer: Apatch-2.0 | Copyright © 2020-present wangxin
---

## 起步

安装：

``` bash
npm install -g qdp-cli
```

创建一个项目：

``` bash
qdp create [app-name] -c
```

[配置参考](./config.md) 

[创建工程](./create.md) 

[代码生成](./generator.md) 
