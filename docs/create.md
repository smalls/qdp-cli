# 创建工程

```bash
qdp create [app-name] -c
````

[可选参数]

|参数|类型|描述|
|:-:|:--:|:--|
|-t,--template|string|用于选择模版，默认为vue|
|-c,--config|boolean|标识模版创建完成后是否生成置文件，默认为true|