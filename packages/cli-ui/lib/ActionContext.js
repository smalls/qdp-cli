module.exports = class ActionContext {
  constructor(request, response) {
    this.request = request;
    this.response = response;
  }
}