const { log, openBrowser } = require('../cli-shared-utils')
const express = require('express');
const http = require('http');
const path = require('path');
const ActionResult = require('./lib/ActionResult');
const ActionContext = require('./lib/ActionContext');

module.exports = class Server {

  constructor(port) {
    const app = express();
    const server = http.createServer(app);
    app.use(express.static(path.join(__dirname, './static')))
    //设置允许跨域访问该服务.
    app.all('*', function (req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'Content-Type');
      res.header('Access-Control-Allow-Methods', '*');
      res.header('Content-Type', 'application/json;charset=utf-8');
      next();
    });
    this.app = app;
    this.server = server;
    this.port = port;
  }

  __handler(handler, request, response) {
    const context = new ActionContext(request, response);
    const { params, query } = request;
    const arg = { ...params, ...query };
    // console.info(arg, params, query);
    const feedback = handler(context, arg);
    if (feedback && feedback.then) {
      feedback.then(res => {
        response.send(new ActionResult().data(res).toJSON());
      })
        .catch(error => {
          response.send(new ActionResult().code(-1).message(error.message).toJSON());
        })
    } else {
      response.send(feedback.toJSON());
    }
  }

  /**
   * 
   * @param {string} path 
   * @param {function(ActionContext):ActionResult} handler
   */
  onAll(path, handler) {
    this.app.all(path, (req, res) => this.__handler(handler, req, res));
  }

  /**
   * 
   * @param {string} path 
   * @param {function(ActionContext):ActionResult} handler
   */
  onGet(path, handler) {
    this.app.get(path, (req, res) => this.__handler(handler, req, res));
  }

  /**
   * 
   * @param {string} path 
   * @param {function(ActionContext):ActionResult} handler
   */
  onPost(path, handler) {
    this.app.post(path, (req, res) => this.__handler(handler, req, res));
  }

  /**
   * 开始监听端口
   * @param {bool} open 是否打开浏览器
   */
  start(open) {
    const url = `http://localhost:${this.port}`
    this.server.listen(this.port, "0.0.0.0", function () {
      log(`🌠  Ready on ${url}`)
      open && openBrowser(url)
    });
  }

  stop() {
    this.server.close();
  }
}