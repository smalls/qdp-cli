const { toHump, getVersions } = require('../../cli-shared-utils/');
module.exports = class Context {
  constructor(options, config) {
    this.version = "1.0"
    this.options = options;
    this.extra = {
      date: new Date().toLocaleString(),
      author: `qdp-cli`,
      version: getVersions().current
    }
    this.config = config;
  }

  packageConfig(res) {
    let pages = [];
    res.forEach(p => {
      const table_name = p.table_name.replace('BLT_', '');
      const tableName = toHump(table_name);
      const apiFix = table_name.replace(/_/g, '/')
      let page = pages.find(p => p.name == tableName);
      if (page == null) {
        page = {
          name: tableName,
          title: p.table_comment,
          icon: "",
          fields: [{ "name": "_index", "editor": false }],
          modify: {},
          apis: {
            query: `/${apiFix}/query`,
            remove: `/${apiFix}/remove`,
            create: `/${apiFix}/create`,
            update: `/${apiFix}/modify`,
            detail: `/${apiFix}/detail`
          }
        };
        pages.push(page);
      }
      const fieldName = toHump(p.col_name);
      page.fields.push({
        name: fieldName,
        text: p.col_comment || fieldName,
      });
    });
    return pages;
  }
}
