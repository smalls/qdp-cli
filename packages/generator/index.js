'use strict'
const GeneratorContext = require('./lib/GeneratorContext');
const { clearConsole, generateTitle, chalk } = require('../cli-shared-utils/');
const path = require('path');
const fs = require('fs');
const executorOptions = {
  config: "",
  template: "",
  savePath: "",
  module: ""
}

async function executor(options = executorOptions) {
  clearConsole(generateTitle());
  console.log();
  console.log(` ${chalk.cyan('Generator')} `);
  console.log();
  console.log(chalk.section('- command arguments'));
  console.log(`${chalk.gray(JSON.stringify(options, null, 2))} `);

  let configFile;
  const miscPath = './misc';
  const configName = options.module || 'config.json';
  const targetPath = options.savePath || './src';

  if (options.config) {
    configFile = options.config;
  } else {
    configFile = `${miscPath}/${configName}`;
  }

  if (!fs.existsSync(configFile)) {
    console.log(chalk.error(`${configFile} not found!`));
    return;
  }
  // 处理输出目录
  let targetDir;
  if (path.isAbsolute(targetPath)) {
    targetDir = targetPath;
  } else {
    targetDir = path.resolve(targetPath);
  }

  const context = new GeneratorContext(options);
  // 构建业务代码

  // 导入配置文件
  console.log();
  console.log(chalk.section('- import configfile'));
  context.importConfig(configFile);
  console.log(` ${chalk.success('complated')} `);

  // 开始生成代码
  console.log();
  console.log(`${chalk.section('- generate')} `);
  try {
    let Generator;
    switch (options.template) {
      case 'lzb':
        Generator = require('./plugins/vue-lzb-generator/index.js');
        break;
      default:
        Generator = require('./plugins/vue-iview-generator/index.js');
        break;
    }
    new Generator(context).generate(targetDir);
  } catch (error) {
    console.log(chalk.error(`[ERROR] generate fail!，${error.message}`));
  }
  // 代码生成完成
  console.log();
  console.log(` ${chalk.success('successfully!')} `);
  console.log();
}

module.exports = (...args) => {
  return executor(...args).catch(err => {
    console.error(` ${chalk.red(err.message)}`);
    process.exit(1)
  })
}
