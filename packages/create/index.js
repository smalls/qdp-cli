'use strict'
const exec = require('child_process').exec
const templates = require('./template.json')
const chalk = require('chalk')
const path = require('path');
const fs = require('fs');

async function executor(projectName, options) {
  const cwd = options.cwd || process.cwd()
  const inCurrent = projectName === '.'
  const name = inCurrent ? path.relative('../', cwd) : projectName
  const targetDir = path.resolve(cwd, projectName || '.')
  options = Object.assign({ template: 'vue', config: true }, options);

  const template = templates[options.template];

  if (!template) {
    console.log(chalk.red(`\n × template [${options.template}] not found!`))
    console.log(chalk.bold('  Supported templates:'))
    Object.keys(templates).forEach(p => {
      const t = templates[p];
      console.log(`  ${chalk.bold.green(p)}\t${chalk.italic.grey(t.desc)}`)
    });
    return;
  }

  const { desc, url, branch } = template;

  console.log(`\n  ${chalk.bold.green(options.template)}\t${chalk.italic.grey(desc)}`);
  if (fs.existsSync(targetDir)) {
    console.log(chalk.red(`\n × [${targetDir}] has existsd!`))
    return;
  }

  // git命令，远程拉取项目并自定义项目名
  let cmdStr = `git clone ${url} ${name} && cd ${name} && git checkout ${branch}`
  exec(cmdStr, (error, stdout, stderr) => {
    if (error) {
      console.log(error)
      process.exit()
    }
    // 判是否生成配置文件
    if (options.config) {
      const create = require("../generator-config/create");
      create(projectName);
    }
    console.log();
    console.log(` ${chalk.success('successfully!')} `);
    console.log();
    console.log(`\n cd ${projectName} && npm install && npm run dev\n`)
    process.exit()
  })
}

module.exports = (...args) => {
  return executor(...args).catch(err => {
    console.error(` ${chalk.red(err.message)}`);
    process.exit(1)
  })
}
