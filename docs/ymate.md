# ymate控制器生成

检索当前目录中的model目录，根据实体生成控制器

```bash
qdp ymate
````

[可选参数]

无

[配置文件]

model目录中增加qdp.json文件

```
// qdp.json
{
  "ignore": ["EntityA","EntityB"]
}
```

```ignore```指定生成控制器时忽略的实体文件名称（不需要带```.java```）
