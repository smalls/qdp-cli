
module.exports = {
  detail() {
    let source = process.env.HOME || process.env.USERPROFILE
    return new ActionResult().data(source);
  }
}
