// const deepObjectMerge = (source, target) => {
//   for (var key in target) {
//     if (source.hasOwnProperty(key) && Object.prototype.toString.call(source[key]) === '[object Object]') {
//       deepObjectMerge(source[key], target[key]);
//     } else {
//       target[key] && (source[key] = target[key]);
//     }
//   }
//   return source;
// }

const deepObjectMerge = (source, target) => {
  for (var key in target) {
    if (source.hasOwnProperty(key)) {
      if (Object.prototype.toString.call(source[key]) === '[object Object]') {
        deepObjectMerge(source[key], target[key]);
      }
    } else {
      source[key] = target[key]
    }
  }
  return source;
}
// deepObjectMerge(settings, options);

exports.merge = (source, target) => {
  const result = {};
  deepObjectMerge(result, target);
  deepObjectMerge(result, source);
  return result;
}
