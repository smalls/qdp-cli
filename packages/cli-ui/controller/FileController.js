const path = require('path');
const fs = require('fs');
const ActionResult = require('../lib/ActionResult');
const hiddenPrefix = '.'

function isDirectory(file) {
  file = file.replace(/\\/g, path.sep)
  try {
    return fs.statSync(file).isDirectory()
  } catch (e) { }
  return false
}

function isHidden(file) {
  try {
    return path.basename(file).charAt(0) === hiddenPrefix;
  } catch { }
  return false
}

function parseYmate(file) {
  try {
    const pomfile = path.resolve(file, './pom.xml');
    if (fs.existsSync(pomfile)) {
      const pomContent = fs.readFileSync(pomfile, 'utf-8');
      const REGEX_VERSION = /<ymate.platform.version>(.+?)<\/ymate\.platform\.version>/;
      if (REGEX_VERSION.test(pomContent)) {
        return {
          version: REGEX_VERSION.exec(pomContent)[1]
        }
      }
    }
  } catch (e) {
    console.info(e.message);
  }
  return undefined
}

function parseVue(file) {
  try {
    const pomfile = path.resolve(file, './src/settings.js');
    if (fs.existsSync(pomfile)) {
      const pomContent = fs.readFileSync(pomfile, 'utf-8');
      const REGEX_VERSION = /version:\s?"(.+?)",/;
      if (REGEX_VERSION.test(pomContent)) {
        return {
          version: REGEX_VERSION.exec(pomContent)[1]
        }
      }
    }
  } catch (e) {
    console.info(e.message);
  }
  return undefined
}

function parseQDP(file) {
  try {
    const dirs = fs.readdirSync(file);
    const vers = dirs.filter(p => p.charAt(0).toLowerCase() === 'v');
    const entity = {};
    vers.forEach(p => {
      const versionDir = path.resolve(file, p);
      let webapi = parseYmate(path.resolve(versionDir, 'webapi'));
      let webapp = parseVue(path.resolve(versionDir, 'webapp'));
      if (webapi || webapp) {
        entity[p] = {
          ymate: webapi,
          vue: webapp
        }
      }
    })
    return Object.keys(entity).length > 0 ? entity : undefined;
  } catch (e) {
    console.info(e.message);
  }
  return undefined
}

function getProjectMate(file) {
  return {
    ymate: parseYmate(file),
    vue: parseVue(file),
    qdp: parseQDP(file),
  }
}

module.exports = {
  userhome() {
    let source = process.env.HOME || process.env.USERPROFILE
    return new ActionResult().data(source);
  },
  
  readdir(context, params = {}) {
    let { source } = params;
    return new Promise((resolve, reject) => {
      if (!source) {
        source = process.env.HOME || process.env.USERPROFILE
      }
      fs.readdir(source, (error, items) => {
        if (error) {
          reject(error);
        } else {
          const dirs = items.filter(p => {
            let dir = path.resolve(source, p)
            return isDirectory(dir) && !isHidden(dir)
          }).map(p => {
            const file = path.resolve(source, p);
            return {
              name: p,
              path: file,
              meta: getProjectMate(file)
            }
          });
          resolve(dirs)
        }
      });
    });
  },

  readfile(context, params = { dir: '', file: '' }) {
    const { dir, file } = params;
    return new Promise((resolve, reject) => {
      let filepath = path.resolve(dir, file);
      fs.readFile(filepath, 'utf8', (error, data) => {
        if (error) {
          reject(error);
        } else {
          let jsonString = data.toString()
            .replace(/\/\*\*.+?\*\//g, '')
            // 清除空白行（对代码没影响）
            .replace(/\n[ ]*\n/g, '\n')
            // .replace(/\r|\n|\t/g, '')
            .replace(/\'/g, '&#39;');
          resolve(JSON.parse(jsonString))
        }
      });
    });
  },

  writefile(context, params = { dir: '', file: '', content: '' }) {
    const { dir, file, content } = params;
    return new Promise((resolve, reject) => {
      let filepath = path.resolve(dir, file);
      fs.writeFile(filepath, content, (error) => {
        if (error) {
          reject(error);
        } else {
          resolve()
        }
      });
    });
  }
}
