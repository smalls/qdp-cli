/**
 * 下划线转换驼峰
 */
exports.toHump = name => {
  return name.replace(/(-|_)(\w)/g, (m, $1, $2) => $2.toUpperCase());
}
/**
 * 驼峰转换下划线
 */
exports.toLine = name => {
  return name.replace(/([A-Z])/g, "_$1").toLowerCase();
}

/**
 * 转大驼峰
 * ab-cd => AbCd
 * ab_cd => AbCd
 */
exports.toCamel = name => {
  return name.replace(/(^|_|-)(\w)/g, (m, $1, $2) => $2.toUpperCase());
}