'use strict'
const { error } = require('../cli-shared-utils')
const ActionResult = require('./lib/ActionResult');
const Server = require('./Server');

const path = require('path');
const fs = require('fs');
async function executor(options) {
  let port = options.port || 3000
  const server = new Server(port);

  const files = fs.readdirSync(path.join(__dirname, './controller'));
  files.forEach(p => {
    const controller = require(path.join(__dirname, './controller', p));
    const root = p
      .replace('Controller.js', '')
      .replace(/([A-Z])/g, "/$1")
      .toLowerCase();
    Object.keys(controller)
      .forEach(p => {
        server.onAll(`${root}/${p}`, controller[p]);
      });
  });

  // require('./controller/FileController')
  //   .readdir({}, { source: '/Users/wangxin/workspace/datong' })
  //   .then(p => {
  //     console.info(JSON.stringify(p, null, 2));
  //   });
  server.onAll("/api/environment", context => {
    var os = require("os");
    var json = {};
    json.temp = os.tmpdir();
    json.endianness = os.endianness();
    json.hostname = os.hostname();
    json.type = os.type();
    json.platform = os.platform();
    json.arch = os.arch();
    json.release = os.release();
    json.uptime = os.uptime();
    json.loadavg = os.loadavg();
    json.totalmem = os.totalmem();
    json.freemem = os.freemem();
    json.cpus = os.cpus();
    json.networkInterfaces = os.networkInterfaces();
    return new ActionResult().data(json);
  });
  // return;
  server.start(false);
}

module.exports = async (...args) => {
  try {
    return executor(...args);
  }
  catch (err) {
    error(err);
    process.exit(1);
  }
}
